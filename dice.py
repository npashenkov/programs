# class definition for an n-sided dice
from random import randrange

class Dice:

   def __init__(self, sides):
      self.sides = sides
      self.value = 1

   def roll(self):
      self.value = randrange(1,self.sides+1)

   def getValue(self):
      return self.value

roll_again = "y"
myDice = Dice(6)

while roll_again == "yes" or roll_again == "y":
    print ("Roll the dice: ")
    myDice.roll()
    print (myDice.getValue())
    roll_again = input("Roll the dice again? ")
