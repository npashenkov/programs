import pygame, sys
from pygame.locals import *

pygame.init()
DisplaySurface = pygame.display.set_mode((400, 300))
pygame.display.set_caption('Hello PyGame!')
while True: # main game loop
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
